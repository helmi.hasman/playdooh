let map;


async function initMap() {
  const { Map } = await google.maps.importLibrary("maps");
  var bounds = new google.maps.LatLngBounds();
  map = new Map(document.getElementById("map"), {
    center: { lat: 3.1319, lng: 101.6841 },
    zoom: 12,
  });

  var sql = "api/get_result.php?";
  var markers = [];
  var marker;

    var eBuildingType = document.getElementById("building_type");
    var eBuildingTypeValue = eBuildingType.value;
    var eSiteName = document.getElementById("site_name");
    var eSiteNameValue = eSiteName.value;

    if(eBuildingTypeValue !== ""){
        sql += "&building_type="+eBuildingTypeValue;
    }

    if(eSiteNameValue !== ""){
        sql += "&location_name="+eSiteNameValue;
    }

    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
    
    var rData = JSON.parse(this.response);
    

  //   var markers = [
  //     ['Linden Lodge Care Home', 3.1319,101.6841],
  //     ['Linden Lodge Residential Home', 52.6009934, -1.6183551],
  //     // ['Linden Grange Residential Home', 52.5486357, -1.5215572],
  // ];

    for(let i = 0; i < rData.data.length; i++) {
        let obj = rData.data[i];
        if(obj.vr_gps !== "-"){
          // markers.push([obj.vr_location_type,obj.vr_gps]);
          console.log(obj.vr_main_type);
          const locArray = obj.vr_gps.split(",");
          console.log(locArray);

          var position = new google.maps.LatLng(locArray[0],locArray[1]);
          bounds.extend(position);
          marker = new google.maps.Marker({
              position: position,
              map: map,
              title: obj.vr_location_type,
          });
          // Automatically center the map fitting all markers on the screen
          map.fitBounds(bounds);
          marker.setMap(map);
        }
       
    }
    }
    xhttp.open("GET", sql);
    xhttp.send();

 



// for( i = 0; i < markers.length; i++ ) {
//   var position = new google.maps.LatLng(markers[i][1]);
//   bounds.extend(position);
//   marker = new google.maps.Marker({
//       position: position,
//       map: map,
//       title: markers[i][0],
//   });
//   // Automatically center the map fitting all markers on the screen
//   map.fitBounds(bounds);
//   marker.setMap(map);
// }

}

// initMap();