function getQuotation() {
    initMap();
    var quoteTable = '<table class="table" id="example1">'+
                        '<thead style="background-color:#3FA9F5;color:white">'+
                                '<tr>'+
                                    '<th class="wd-1" style="color:white">No</th>'+
                                    '<th style="color:white">Site Code</th>'+
                                    '<th style="color:white">LED Screens</th>'+
                                    '<th style="color:white">Location</th>'+
                                    '<th style="color:white">Content Duration Per Slot</th>'+
                                    '<th style="color:white">Height x Width</th>'+
                                    '<th style="color:white">Total Sq Feet</th>'+
                                    '<th style="color:white">Min Exposure Per Day</th>'+
                                    '<th style="color:white">Traffic/Vehicles Montly</th>'+
                                    '<th style="color:white">Cost (MYR)</th>'+
                                    '</tr></thead><tbody>';

    var smallQuoteTable = "";
    var maps_location = "";
    var sql = "api/get_result.php?";

    var eBuildingType = document.getElementById("building_type");
    var eBuildingTypeValue = eBuildingType.value;
    var eSiteName = document.getElementById("site_name");
    var eSiteNameValue = eSiteName.value;

    if(eBuildingTypeValue !== ""){
        sql += "&building_type="+eBuildingTypeValue;
    }

    if(eSiteNameValue !== ""){
        sql += "&location_name="+eSiteNameValue;
    }
    // if (value == "") {
    //   document.getElementById("txtHint").innerHTML = "";
    //   return;
    // }

    // alert(sql);
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
    
    var rData = JSON.parse(this.response);
    var totalMinExposures = 0;
    var totalReach = 0;
    var totalRate = 0;
    var totalScreens = 0;

    for(let i = 0; i < rData.data.length; i++) {
        let obj = rData.data[i];

        quoteTable += '<tr class="border-bottom">';
        quoteTable += '<td>'+(i+1)+'</td>';
        quoteTable += '<td>'+obj.vr_code+'</td>';
        quoteTable += '<td>'+obj.vr_location_type+'</td>';
        quoteTable += '<td>'+obj.vr_location_name+'</td>';
        quoteTable += '<td>'+obj.vr_duration_per_ads+' seconds</td>';
        quoteTable += '<td>'+obj.vr_screen_size+'</td>';
        quoteTable += '<td>'+obj.vr_total_sqfeet+'</td>';
        quoteTable += '<td>'+obj.vr_min_exposures+'</td>';
        quoteTable += '<td>'+obj.vr_reach_month+'</td>';
        quoteTable += '<td>'+obj.vr_publish_1+'</td>';
        quoteTable += '</tr>';

        totalMinExposures += parseInt(obj.vr_min_exposures);
        totalReach += parseInt(obj.vr_reach_month);
        totalRate += parseFloat(obj.vr_publish_1);
        totalScreens += parseInt(obj.vr_no_of_unit);

        var filename = "assets/img/billboards/"+obj.vr_code.replaceAll(' ', '')+".png";
        // var imagefile = new File(filename);
        // console.log(imagefile);
        // var srcname = "";
        // if(imagefile.exists()){
        //     srcname = filename;
        // }
        // else{
        //     srcname = "assets/img/billboards/no_img.jpg";
        // }
        
        // console.log(srcname);

        //Maps locations
        maps_location  += '<div class="card custom-card">';
        maps_location  += '<div class="card-body p-3">';
        maps_location  += '<div class="row g-0 blog-list">';
        maps_location  += '<div class="col-xl-5 col-lg-12 col-md-12"  style="padding:0 !important;">';
        maps_location  += '<div class="card-body p-0">';
        maps_location  += '<div class="item-card-img">';
        maps_location  += '<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black">';
        maps_location  += '<img style="object-fit: cover;height:180px" src="'+filename+'" alt="" onerror="javascript:this.src="assets/img/billboards/no_img.jpg">';
        // maps_location  += '<img style="object-fit: cover;height:180px" src="assets/img/billboards/no_img.jpg" alt="">';
        maps_location  += '</a></div></div></div>';
																
        maps_location  += '<div class="col-xl-7 col-lg-12 col-md-12">';
        maps_location  += '<div class="card-body p-2">';
        maps_location  += '<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black"><h5 class="font-weight-semibold mt-3">'+obj.vr_location_type+'</h5></a>';
        maps_location  += '<h6 class="font-weight-semibold mt-3">'+obj.vr_code+'</h6>';
        maps_location  += '<p class=""></p>';
        maps_location  += '<div class="item-card-desc d-flex">';
        maps_location  += '<div class="main-contact-body">';
        maps_location  += '<img src="assets/img/icons/view.png" style="width:25px"/><br><span>'+obj.vr_reach_month+'</span>';
        maps_location  += '</div>';
        maps_location  += '<div class="main-contact-body">';
        maps_location  += '<img src="assets/img/icons/scale-up.png" style="width:25px"/><br><span>'+obj.vr_screen_size+'</span>';
        maps_location  += '</div>';
        maps_location  += '<div class="main-contact-body">';
        maps_location  += '<img src="assets/img/icons/compass.png" style="width:25px"/><br><span>'+obj.vr_location_name+'</span>';
        maps_location  += '</div>';
        maps_location  += '<div class="main-contact-body">';
        maps_location  += '<img src="assets/img/icons/seen.png" style="width:25px"/><br><span style="color:green">Available</span>';
        maps_location  += '</div></div>';
        maps_location  += '<div class="col-xl-12 col-lg-12 col-md-12">';
        maps_location  += '<button class="btn ripple btn-block btn-1" style="color:white"><i class="fe fe-plus"></i>&nbsp;Add Sign</button>';
        maps_location  += '</div></div></div></div></div></div>';

        

    }

    document.getElementById("map_locations").innerHTML = maps_location;

        quoteTable += '<tr class="border-bottom">';
        quoteTable += '<td colspan="4"><b>Total</b></td>';
        quoteTable += '<td></td>';
        quoteTable += '<td></td>';
        quoteTable += '<td></td>';
        quoteTable += '<td><b>'+totalMinExposures+'</b></td>';
        quoteTable += '<td><b>'+totalReach+'</b></td>';
        quoteTable += '<td><b>'+totalRate.toFixed(2)+'</b></td>';
        quoteTable += '</tr>';
        quoteTable += '</tbody></table>';

        document.getElementById("quotation_table").innerHTML = quoteTable;


        smallQuoteTable += '<table class="table" class="table" id="example1" style="width:40%">';

        smallQuoteTable += '<tr >';
        smallQuoteTable += '<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Project Cost</td>';
        smallQuoteTable += '<td style="text-align: end;">'+totalRate.toFixed(2)+'</td>';
        smallQuoteTable += '</tr>';

        smallQuoteTable += '<tr >';
        smallQuoteTable += '<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Discounted Rate</td>';
        smallQuoteTable += '<td style="text-align: end;">0</td>';
        smallQuoteTable += '</tr>';

        smallQuoteTable += '<tr >';
        smallQuoteTable += '<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Project Cost</td>';
        smallQuoteTable += '<td style="text-align: end;">80%</td>';
        smallQuoteTable += '</tr>';

        smallQuoteTable += '<tr >';
        smallQuoteTable += '<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Insights (pre & Post) - 2 Campaigns</td>';
        smallQuoteTable += '<td style="text-align: end;">Included in the package</td>';
        smallQuoteTable += '</tr>';

        smallQuoteTable += '<tr >';
        smallQuoteTable += '<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Content development adn adaptation</td>';
        smallQuoteTable += '<td style="text-align: end;">0</td>';
        smallQuoteTable += '</tr>';

        smallQuoteTable += '<tr ><td></td><td></td></tr>';

        smallQuoteTable += '<tr >';
        smallQuoteTable += '<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Comitted Ad Slots</td>';
        smallQuoteTable += '<td style="text-align: end;">0</td>';
        smallQuoteTable += '</tr>';

        var cpm_average = totalRate/rData.data.length;
        document.getElementById("small_quotation_table").innerHTML = smallQuoteTable;
        
        document.getElementById("quotation_title").innerHTML = 'RM'+abbrNum(totalRate,1);

        document.getElementById("impressions_title").innerHTML = abbrNum(totalReach,1);

        document.getElementById("total_screens").innerHTML = abbrNum(totalScreens,1);

        document.getElementById("total_venue_type").innerHTML = abbrNum(rData.data.length,1);

        document.getElementById("cpm_average").innerHTML = 'RM'+abbrNum(cpm_average,1);

        

        

    }
    xhttp.open("GET", sql);
    xhttp.send();
  }


function abbrNum(number, decPlaces) {
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "K", "M", "B", "T" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number) {
             // Here, we multiply by decPlaces, round, and then divide by decPlaces.
             // This gives us nice rounding to a particular decimal place.
             number = Math.round(number*decPlaces/size)/decPlaces;

             // Handle special case where we round up to the next abbreviation
             if((number == 1000) && (i < abbrev.length - 1)) {
                 number = 1;
                 i++;
             }

             // Add the letter for the abbreviation
             number += abbrev[i];

             // We are done... stop
             break;
        }
    }

    return number;
}