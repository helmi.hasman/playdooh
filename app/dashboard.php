<?php
session_start();
if(!isset($_SESSION['vr_email']) && empty($_SESSION['vr_email'])) {
	echo "<script>";
	//echo "alert('Welcome!');";
	echo "window.location = ' index.html'"; // redirect with javascript, after page loads
	echo "</script>";
 }
?>
<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
		<meta name="description" content="Play/Dooh -  Admin Panel HTML Dashboard Template">
		<meta name="author" content="Spruko Technologies Private Limited">
		<meta name="keywords" content="admin,dashboard,panel,bootstrap admin template,bootstrap dashboard,dashboard,themeforest admin dashboard,themeforest admin,themeforest dashboard,themeforest admin panel,themeforest admin template,themeforest admin dashboard,cool admin,it dashboard,admin design,dash templates,saas dashboard,dmin ui design">

		<!-- Favicon -->
		<link rel="icon" href="assets/img/icons/vr_logo.jpg" type="image/x-icon"/>

		<!-- Title -->
		<title>Play/Dooh</title>

		<!-- Bootstrap css-->
		<link  id="style" href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		
		<!-- Icons css-->
		<link href="assets/plugins/web-fonts/icons.css" rel="stylesheet"/>
		<link href="assets/plugins/web-fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
		<link href="assets/plugins/web-fonts/plugin.css" rel="stylesheet"/>

		<!-- Style css-->
		<link href="assets/css/style.css" rel="stylesheet">

		<!-- Select2 css-->
		<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet">

		<!-- Mutipleselect css-->
		<link rel="stylesheet" href="assets/plugins/multipleselect/multiple-select.css">

		<!-- Internal Morrirs Chart css-->
		<link href="assets/plugins/morris.js/morris.css" rel="stylesheet">
		
		<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

		<style>
			.scroll-div{
				padding:5px;
				margin:5px;
				overflow-y: auto;
				overflow-x: hidden;
				text-align:justify;
				height:500px;
				}
			.btn-1 {
			background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);
			border: none;
			}
			.leftbox {
                float:left;
                width:33%;
            }
            .middlebox{
                float:left;
                width:33%;
            }
            .rightbox{
                float:right;
                width:33%;
            }
            /* 
             * Always set the map height explicitly to define the size of the div element
             * that contains the map. 
             */
            #map {
              height: 100%;
              width: 100%;
            }
		  </style>

	</head>

	<body class="ltr main-body leftmenu">

		<!-- Loader -->
		<div id="global-loader">
			<img src="assets/img/loader.svg" class="loader-img" alt="Loader">
		</div>
		<!-- End Loader -->

		<!-- Page -->
		<div class="page">

		<!-- Main Header-->
		<div class="main-header sticky">
			<div class="main-container container-fluid">
				<div class="main-header-left" style=" display: flex;align-items: left;">
					<a href="index.html"><img src="assets/img/icons/vr_logo2.png" class="mobile-logo" alt="logo" style="height:50px"></a>
				</div>
				<div class="main-header-center">
					
					
				</div>
				<div class="main-header-right">
					<button class="navbar-toggler navresponsive-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4"
						aria-expanded="false" aria-label="Toggle navigation">
						<i class="fe fe-more-vertical header-icons navbar-toggler-icon"></i>
					</button><!-- Navresponsive closed -->
					
					<div class="navbar navbar-expand-lg  nav nav-item  navbar-nav-right responsive-navbar navbar-dark  ">
						<div class="collapse navbar-collapse" id="navbarSupportedContent-4">
							<div class="d-flex order-lg-2 ms-auto">
								<div class="main-header-notification">
									<a class="nav-link" href="campaign.html" style="font-size: small;">
										<i class="header-icons"></i>
										Campaigns
									</a>
								</div>
								<div class="main-header-notification">
									<a class="nav-link" href="" style="font-size: small;">
										<i class="header-icons"></i>
										Account Settings
									</a>
								</div>
								<div class="main-header-notification">
									<a class="nav-link" href="index.html" style="font-size: small;">
										<i class="fe fe-user header-icons"></i>
										Logout
									</a>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Main Header-->

			<!-- Main Content-->
			<div class="main-content pt-0">

				<div class="main-container container-fluid">
					<div class="inner-body">

						<!-- Page Header -->
						<div class="page-header">
							<!-- Row -->
											
							<div class="col-lg-1 col-md-1">
								&nbsp;
							</div>
							<div class="col-lg-10 col-md-10">
								<div class="card custom-card">
									<div class="card-body">
										<div class="row row-sm">
											<div class="col-md-1">
												<a class="btn ripple btn-1" style="color:white">Save</a>
											</div>
											<div class="col-md-3">
												<input class="form-control" placeholder="Client Name" type="text">
											</div>
											<div class="col-md-3">
												<input class="form-control" placeholder="Campaign Name" type="text">
											</div>
											<div class="col-md-3">
												<input class="form-control" placeholder="Tenure" type="text">
											</div>
											<div class="col-lg">
												<button class="btn ripple btn-1" style="color:white">Share</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-1 col-md-1">
								&nbsp;
							</div>
						
						<!-- End Row -->

						</div>
						<!-- End Page Header -->

						<!-- row -->
						
						<div class="row row-sm">
							<div style="width:12%">
								&nbsp;
							</div>
							<div class="col-lg-6 col-xl-6 col-xxl-6 col-md-6 col-6" >
								<h4 class="text-center tx-15">Key Metrics</h4>
								<div id="key_metrics_box" class="card custom-card text-center" style="background-color: black;color:white">
									<div class="card-body pb-3">
										<div class="d-flex">
											<div class="">
												<h4 class="mb-2" id="total_screens_title">0</h4>
												<div class="d-flex tx-12"> Total Screens No</div>
											</div>
											<div style="width:5%;"></div>
											<div class="">
												<h4 class="mb-2" id="average_sqfeet_title">0</h4>
												<div class="d-flex tx-12">Average Sq Feet</div>
											</div>
											<div style="width:5%;"></div>
											<div class="">
												<h4 class="mb-2" id="total_sqfeet_title">0</h4>
												<div class="d-flex tx-12">Total Sq Feet</div>
											</div>
											<div style="width:5%;"></div>
											<div class="">
												<h4 class="mb-2" id="min_exposure_title">0</h4>
												<div class="d-flex tx-12">Min Exposures Per Day</div>
											</div>
											<div style="width:5%;"></div>
											<div class="">
												<h4 class="mb-2" id="traffic_title">0</h4>
												<div class="d-flex tx-12">Traffic/Vechicles</div>
											</div>
											<div style="width:5%;"></div>
											<div class="">
												<h4 class="mb-2" id="reach_title">0</h4>
												<div class="d-flex tx-12">Reach/Eyeballs</div>
											</div>
											<div style="width:5%;"></div>
											<!-- <a aria-controls="collapseExample" aria-expanded="false" data-bs-toggle="collapse" href="#collapseExample" style="color:white" onclick="openKeyMetricsBox()"> -->
											
											<a href="#" style="color:white;" onclick="openKeyMetricsBox()">
												<div class="">
													<h6 class="tx-15" id="total_venue_type">Details</h6>
													<div class="tx-12" style="text-align: center;">
														<i id="key_metrics_box_carret" class="fas fa-caret-down ms-1"></i>
													</div>
												</div>
											</a>

											<!-- <a href="#" style="color:white" onclick="openKeyMetricsBox()">
												<div>
													<h6 class="tx-15">Details</h6>
													<div class="text-center">
														<i id="key_metrics_box_carret" class="fas fa-caret-down ms-1"></i>
													</div>
												</div>
											</a> -->
											
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-xl-2 col-xxl-2 col-md-2 col-12">
								<h4 class="tx-15 text-center">Quotation</h4>
								<div id="quotation_box" class="card custom-card text-center" style="background-color: black;color:white">
									<div class="card-body pb-3">
										
										<div class="d-flex">
											<div class="">
												<h4 class="mb-2" id="quotation_title">RM0</h4>
												<div class="d-flex tx-12"> Total</div>
											</div>
											<div style="width:40%;"></div>
											<!-- <a aria-controls="collapse2" aria-expanded="false" data-bs-toggle="collapse" href="#collapse2" style="color:white" onclick="openQuotationBox()"> -->
											<a href="#" style="color:white" onclick="openQuotationBox()">
												<div>
													<h6 class="tx-15">Details</h6>
													<div class="text-center"><i id="quotation_box_carret" class="fas fa-caret-down ms-1"></i></div>
												</div>
											</a>
											
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-xl-1 col-xxl-1 col-md-1 col-12">
								<h4 class="tx-15 text-center">Locations</h4>
								<div id="location_box" class="card custom-card text-center">
									<div class="card-body pb-1">
										<!-- <a aria-controls="collapse3" aria-expanded="false" data-bs-toggle="collapse" href="#collapse3" > -->
										<a href="#" onclick="openLocationBox()">
											<div class="">
												<img id="pic1" class="pic-1" alt="product-image-1" src="assets/img/icons/map.png" style="width:50px">
												<div class="text-center"><i id="location_box_carret" class="fas fa-caret-down"></i></div>
											</div>
										</a>
									</div>
								</div>
							</div>
						
						</div>
						<!-- End row -->
						<div class="collapse mg-t-5" id="collapseExample">
							<div class="row row-sm">
								<!-- <div class="col-md-12 col-lg-12 col-xl-12"> -->
									<div class="card custom-card transcation-crypto">
										<div class="card-body">

											<div class="leftbox"> 
												<div>
													<h5>Reach/Eyeballs - Daily and Monthly</h5>
													<canvas id="chart_one"></canvas>
												</div>
											</div>
											<div class="middlebox"> 
												<div>
													<h5>Traffic Light</h5>
													<canvas id="chart_two"></canvas>
												</div>
											</div>
											<div class="rightbox"> 
												<div>
													<h5>Traffic/Vehicles - Daily and Monthly</h5>
													<canvas id="chart_three"></canvas>
												</div>
											</div>

										</div>
										
									</div>

								<!-- </div> -->
							</div>
							<!-- <img src="assets/img/dooh_chart.png" /> -->
						</div>
						<!-- Row -->
						<div class="collapse mg-t-5" id="collapse2">
							<div class="row row-sm">
								<div class="col-md-12 col-lg-12 col-xl-12">
									<div class="card custom-card transcation-crypto">
										
										<div class="card-body">
											<div class="table-responsive">
												<div id="quotation_table"></div>

												<!-- <table class="table" id="example1">
													<thead style="background-color:#3FA9F5;color:white">
														<tr>
															<th class="wd-1" style="color:white">No</th>
															<th style="color:white">Site Code</th>
															<th style="color:white">LED Screens</th>
															<th style="color:white">Location</th>
															<th style="color:white">Jul-23</th>
															<th style="color:white">Sep-23</th>
															<th style="color:white">Nov-23</th>
															<th style="color:white">Dec-23</th>
															<th style="color:white">Mar-23</th>
															<th style="color:white">Comitted Ad Slots</th>
															<th style="color:white">Content Duration Per Slot</th>
															<th style="color:white">Height</th>
															<th style="color:white">Width</th>
															<th style="color:white">Total Sq Feet</th>
															<th style="color:white">Min Exposure Per Day</th>
															<th style="color:white">Traffic/Vehicles Daily</th>
															<th style="color:white">Traffic/Vehicles Montly</th>
															<th style="color:white">Cost (MYR)</th>
															
														</tr>
													</thead>
													<tbody>
														<tr class="border-bottom">
															<td>1</td>
															<td class="font-weight-bold">VR2003</td>
															<td>Jalan Damansara, Bukit Damansara</td>
															<td>Kuala Lumpur</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>25</td>
															<td>15 seconds</td>
															<td>1200</td>
															<td>240</td>
															<td>720</td>
															<td>1080</td>
															<td>44509</td>
															<td>1135270</td>
															<td>40,000.00</td>
															
														</tr>
														<tr class="border-bottom">
															<td>2</td>
															<td class="font-weight-bold">VR0003</td>
															<td>KL South, Jalan Puchong</td>
															<td>Bangsar</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>25</td>
															<td>15 seconds</td>
															<td>1200</td>
															<td>720</td>
															<td>240</td>
															<td>1080</td>
															<td>44509</td>
															<td>1135270</td>
															<td>30,000.00</td>
															
														</tr>
														<tr class="border-bottom">
															<td>3</td>
															<td class="font-weight-bold">VR3001</td>
															<td>Jalan Raja Laut</td>
															<td>Kuala Lumpur</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>25</td>
															<td>15 seconds</td>
															<td>1200</td>
															<td>720</td>
															<td>240</td>
															<td>1080</td>
															<td>44509</td>
															<td>1135270</td>
															<td>31,250.00</td>
															
														</tr>
														<tr class="border-bottom">
															<td>4</td>
															<td class="font-weight-bold">VR0301</td>
															<td>Jalan Semangat, Petaling Jaya</td>
															<td>Damansara, Kuala Lumpur</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>25</td>
															<td>15 seconds</td>
															<td>1200</td>
															<td>720</td>
															<td>240</td>
															<td>1080</td>
															<td>44509</td>
															<td>1135270</td>
															<td>50,000.00</td>
															
														</tr>
														<tr class="border-bottom">
															<td>5</td>
															<td class="font-weight-bold">VR3002</td>
															<td>Jalan Tun Sambanthan, near KL Sentral</td>
															<td>Kuala Lumpur</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>5</td>
															<td>25</td>
															<td>15 seconds</td>
															<td>1200</td>
															<td>720</td>
															<td>240</td>
															<td>1080</td>
															<td>44509</td>
															<td>1135270</td>
															<td>60,000.00</td>
															
														</tr>

														<tr class="border-bottom">
															<td colspan="4"><b>Total</b></td>
															<td><b>176</b></td>
															<td><b>176</b></td>
															<td><b>176</b></td>
															<td><b>176</b></td>
															<td><b>176</b></td>
															<td><b>880</b></td>
															<td></td>
															<td></td>
															<td></td>
															<td><b>23,922.24</b></td>
															<td><b>55792</b></td>
															<td><b>1,900,010</b></td>
															<td><b>117,000,289</b></td>
															<td><b>1,315,000.00</b></td>
															
														</tr>
													</tbody>
												</table> -->

												<div id="small_quotation_table"></div>
												<!-- <table class="table" class="table" id="example1" style="width:40%">
													<tr >
														<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Project Cost</td>
														<td style="text-align: end;">6,575,000.00</td>
													</tr>
													<tr >
														<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Discounted Rate</td>
														<td style="text-align: end;">1,315,000.00</td>
													</tr>
													<tr >
														<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Project Cost</td>
														<td style="text-align: end;">80%</td>
													</tr>
													<tr >
														<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Insights (pre & Post) - 2 Campaigns</td>
														<td style="text-align: end;">Included in the package</td>
													</tr>
													<tr >
														<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Content development adn adaptation</td>
														<td style="text-align: end;">93,750.00</td>
													</tr>
													<tr >
														<td></td>
														<td></td>
													</tr>
													<tr >
														<td style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);color:black">Comitted Ad Slots</td>
														<td style="text-align: end;">880</td>
													</tr>

												</table> -->
											</div>
											<br>
											<!-- <a class="btn btn-1" style="color:white" onclick="openSummaryBox()">Summary</a>
											<br>
											<br>
											<div class="table-responsive" id="summaryBox" style="display:none">
												<table class="table" id="example2">
													<thead style="background-color:#3FA9F5;color:white">
														<tr>
															<th style="color:white;width:60%">&nbsp;</th>
															<th style="color:white">Mega</th>
															<th style="color:white">Micro</th>
															<th style="color:white">Total</th>
															
															
														</tr>
													</thead>
													<tbody>
														<tr class="border-bottom">
															<td>Min. Exposure/day</td>
															<td>55,792</td>
															<td>35,572</td>
															<td>91,364</td>
															
															
														</tr>
														<tr class="border-bottom">
															<td>Media cost</td>
															<td>1,315,000</td>
															<td>800,100</td>
															<td>2,115,100</td>
															
															
														</tr>
														<tr class="border-bottom">
															<td>Content development, adaptations, content management and uploading, campaign audit & reporting</td>
															<td>93,750</td>
															<td>56,250</td>
															<td>150,000</td>
															
															
														</tr>
														<tr class="border-bottom">
															<td>Pre and Post campaign insights</td>
															<td>Not Included in the package</td>
															<td>Not Included in the package</td>
															<td>-</td>
															
															
														</tr>
														<tr class="border-bottom">
															<td>Total Cost before tax</td>
															<td>1,408,750</td>
															<td>856,350</td>
															<td>2,265,100</td>
														</tr>
														<tr class="border-bottom">
															<td>6% SST</td>
															<td>84,525</td>
															<td>51,381</td>
															<td>135,906</td>
														</tr>
														<tr class="border-bottom">
															<td>Cost inclusive SST</td>
															<td>1,493,275</td>
															<td>907,731</td>
															<td>2,401,006</td>
														</tr>
														<tr class="border-bottom">
															<td>Commiited ad slot</td>
															<td>880</td>
															<td>540</td>
															<td>1,420</td>
														</tr>
														<tr class="border-bottom">
															<td>Cost/Slot (pure media cost)</td>
															<td>1,494</td>
															<td>1,482</td>
															<td>2,976</td>
														</tr>
														<tr class="border-bottom">
															<td>Cost/Slot (end to end)</td>
															<td>1,697</td>
															<td>1,681</td>
															<td>3,378</td>
														</tr>
														<tr class="border-bottom">
															<td>Others Included in the Package as FOC<br>
																1) Website Traffic<br>
																2) Audit & Reporting<br>
																3) Competitors Analysis
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														
													</tbody>
												</table>
											</div> -->
										</div>
									</div>
									<!-- Row End -->
								</div>
							</div>
							
						</div>
						<!-- End row -->
						<div class="collapse mg-t-5" id="collapse3">
							<div class="card custom-card">
								<div class="row row-sm">
									<div class="col-xl-6 col-lg-6 col-sm-6 pe-0 ps-0 border-end">	
										<div id="map"></div>
											<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1991.880709041158!2d101.71099650800299!3d3.157485099425247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc37d12d669c1f%3A0xc955b08cfc1aae29!2sSuria%20KLCC!5e0!3m2!1sen!2smy!4v1681234604303!5m2!1sen!2smy" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> -->
									</div>
									<div class="col-xl-6 col-lg-6 col-sm-6 pe-0 ps-0 border-end">
											<div class="scroll-div">
												<div class="col-xl-12 col-lg-12 col-md-12">
													<div id="map_locations"></div>
													<!-- <div class="card custom-card">
														<div class="card-body p-3">
															<div class="row g-0 blog-list">
																<div class="col-xl-5 col-lg-12 col-md-12"  style="padding:0 !important;">
																	<div class="card-body p-0">
																		<div class="item-card-img">
																			<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black">
																				<img style="object-fit: cover;height:180px" src="assets/img/billboards/board1.png" alt="">
																			</a>
																		</div>
																	</div>
																</div>
																
																	<div class="col-xl-7 col-lg-12 col-md-12">
																			<div class="card-body p-2">
																				<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black"><h5 class="font-weight-semibold mt-3">Jalan Damansara</h5></a>
																				<h6 class="font-weight-semibold mt-3">VR 2003</h6>
																				<p class=""></p>
																				<div class="item-card-desc d-flex">
																					<div class="main-contact-body">
																						<img src="assets/img/icons/view.png" style="width:25px"/><br><span>680.1K</span>
																					</div>
																					<div class="main-contact-body">
																						<img src="assets/img/icons/scale-up.png" style="width:25px"/><br><span>846x234</span>
																					</div>
																					<div class="main-contact-body">
																						<img src="assets/img/icons/compass.png" style="width:25px"/><br><span>North</span>
																					</div>
																					<div class="main-contact-body">
																						<img src="assets/img/icons/seen.png" style="width:25px"/><br><span style="color:green">Available</span>
																					</div>
																				</div>
																				<div class="col-xl-12 col-lg-12 col-md-12">
																					<button class="btn ripple btn-block btn-1" style="color:white"><i class="fe fe-plus"></i>&nbsp;Add Sign</button>
																				</div>
																				
																			</div>
																	
																	</div>
																
															</div>
														</div>
													</div>
													<div class="card custom-card">
														<div class="card-body p-3">
															<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black">
															<div class="row g-0 blog-list">
																<div class="col-xl-5 col-lg-12 col-md-12"  style="padding:0 !important;">
																	<div class="card-body p-0">
																		<div class="item-card-img">
																			<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black">
																				<img style="object-fit: cover;height:180px" src="assets/img/billboards/board2.png" alt="">
																			</a>
																		</div>
																	</div>
																</div>
																<div class="col-xl-7 col-lg-12 col-md-12">
																	<div class="card-body p-2">
																		<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black"><h5 class="font-weight-semibold mt-3">KL South, Jalan Puchong</h5></a>
																		<h6 class="font-weight-semibold mt-3">VR 2014</h6>
																		<p class=""></p>
																		<div class="item-card-desc d-flex">
																			<div class="main-contact-body">
																				<img src="assets/img/icons/view.png" style="width:25px"/><br><span>680.1K</span>
																			</div>
																			<div class="main-contact-body">
																				<img src="assets/img/icons/scale-up.png" style="width:25px"/><br><span>846x234</span>
																			</div>
																			<div class="main-contact-body">
																				<img src="assets/img/icons/compass.png" style="width:25px"/><br><span>North</span>
																			</div>
																			<div class="main-contact-body">
																				<img src="assets/img/icons/seen.png" style="width:25px"/><br><span style="color:green">Available</span>
																			</div>
																		</div>
																		<div class="col-xl-12 col-lg-12 col-md-12">
																			<button class="btn ripple btn-block btn-1" style="color:white"><i class="fe fe-plus"></i>&nbsp;Add Sign</button>
																		</div>
																		
																	</div>
																</div>
															</div>
														
														</div>
													</div>
													<div class="card custom-card">
														<div class="card-body p-3">
															
																<div class="row g-0 blog-list">
																	<div class="col-xl-5 col-lg-12 col-md-12"  style="padding:0 !important;">
																		<div class="card-body p-0">
																			<div class="item-card-img">
																				<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black">
																					<img style="object-fit: cover;height:180px" src="assets/img/billboards/board3.png" alt="">
																				</a>
																				
																			</div>
																		</div>
																	</div>
																	<div class="col-xl-7 col-lg-12 col-md-12" >
																		<div class="card-body p-2">
																			<a data-bs-target="#modaldemo6" data-bs-toggle="modal" href="" style="color:black"><h5 class="font-weight-semibold mt-3">Jalan Raja Laut</h5></a>
																			<h6 class="font-weight-semibold mt-3">VR 2014</h6>
																			<p class=""></p>
																			<div class="item-card-desc d-flex">
																				<div class="main-contact-body">
																					<img src="assets/img/icons/view.png" style="width:25px"/><br><span>680.1K</span>
																				</div>
																				<div class="main-contact-body">
																					<img src="assets/img/icons/scale-up.png" style="width:25px"/><br><span>846x234</span>
																				</div>
																				<div class="main-contact-body">
																					<img src="assets/img/icons/compass.png" style="width:25px"/><br><span>North</span>
																				</div>
																				<div class="main-contact-body">
																					<img src="assets/img/icons/seen.png" style="width:25px"/><br><span style="color:green">Available</span>
																				</div>
																			</div>
																			<div class="col-xl-12 col-lg-12 col-md-12">
																				<button class="btn ripple btn-block btn-1" style="color:white"><i class="fe fe-plus"></i>&nbsp;Add Sign</button>
																			</div>
																			
																		</div>
																	</div>
																</div>	
															
														</div>
													</div> -->
												</div>
											</div>
										
									</div>
									
								</div>

							</div>
							
							
						</div>
						<!-- Row -->
						<div class="row row-sm">
							<div class="col-md-12">
								<div class="card custom-card">
									<div class="row row-sm">
										<div class="col-xl-12 col-lg-12 col-sm-12">
											<div class="card-body">
												<h4 class="mb-0">Site Attributes</h4>
												<br>
												<div class="row row-sm">
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Media Platform</span>
														<div class="form-group ">
															<select name="building_type" id="building_type" class="form-control select2" onchange="getQuotation()">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Site code</span>
														<div class="form-group ">
															<select name="site_name" id="site_name" class="form-control select2" onchange="getQuotation()">
																<option value="">Default Select</option>
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<!-- <span style="text-align: start;">City</span>
														<div class="form-group ">
															<select name="location_name" id="location_name" class="form-control select2">
																<option value="">Default Select</option>
																<option value="Kuala Lumpur">Kuala Lumpur</option>
																<option value="Selangor">Selangor</option>
															</select>
														</div> -->
													</div>
												</div>
												<div class="row row-sm">
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">State</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">District</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Road/Site</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
															
															</select>
														</div>
													</div>
												</div>
												<div class="row row-sm">
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Orientation</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Location: Urban/ Other Urban/ Rural</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Location: Commercial/ Arterial/ Neighbourhood</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row row-sm">
									    <a href="#/" onclick="openImpactBox()" style="color:black"><h5 class="mb-2">&nbsp;&nbsp;&nbsp;<img id="impact_arrow" src="assets/img/icons/arrow_right.png" height="20px"/>Optimization through Impact Attributes</h5></a>
										<div id="impact_box" class="col-xl-12 col-lg-12 col-sm-12" style="display:none">
											<div class="card-body">
												<h4 class="mb-0">Impact Attributes</h4>
												<br>
												<div class="row row-sm">
													<div class="form-group col-lg-3 ">
														<span style="text-align: start;">Dwell Time</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-3 ">
														<span style="text-align: start;">Angle of view</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
															
															</select>
														</div>
													</div>
													<div class="form-group col-lg-3 ">
														<span style="text-align: start;">Board Viewing Distance</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-3 ">
														<span style="text-align: start;">Competing Screens</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>

									<div class="row row-sm">
										<a href="#/" onclick="openDemoBox()" style="color:black"><h5 class="mb-2">&nbsp;&nbsp;&nbsp;<img id="demo_arrow" src="assets/img/icons/arrow_right.png" height="20px"/>Optimization through Demographics Attributes</h5></a>
										<div id="demo_box" class="col-xl-12 col-lg-12 col-sm-12" style="display:none">
											<div class="card-body">
												<h4 class="mb-0">Demographics Attributes</h4>
												<br>
												<div class="row row-sm">
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Affluence</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Age Bands</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Buyer Behaviour</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
												</div>
												<div class="row row-sm">
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Family Units</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Gender</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Hobby</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
												</div>
												<div class="row row-sm">
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Millennial Gender</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Tourist & Travelers</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
													<div class="form-group col-lg-4 ">
														<span style="text-align: start;">Mobile device</span>
														<div class="form-group ">
															<select name="country" class="form-control select2">
																<option value="">Default Select</option>
																
															</select>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<!-- Grid modal -->
								<div class="modal" id="modaldemo6">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content modal-content-demo">
											
											<div class="modal-body">
												<div class="row row-sm">
													<div class="col-md-3">
														<div class="item-card-img">
															<img style="object-fit: cover;height:150px" src="assets/img/billboards/board2.png" alt="">
														
														</div>
													</div>
													<div class="col-md-7">
														<h5 class="font-weight-bold mt-0">Jalan Raja Laut</h5>
														<p class="font-weight-semibold mt-0">VR 2014</p>
														<p>3.1334 &deg;N, 101.629320 &deg;E</p>
														<img src="assets/img/icons/view.png" style="width:20px"/> &nbsp;<span>360,000 Average Daily Views</span><br>
														<img src="assets/img/icons/day-and-night.png" style="width:20px"/> &nbsp;<span>2160 per day</span><br>
														<img src="assets/img/icons/road.png" style="width:20px"/> &nbsp;<span>Left side of the road</span><br>
														<img src="assets/img/icons/stopwatch.png" style="width:20px"/> &nbsp;<span>15 seconds</span><br>
														<img src="assets/img/icons/scale-up.png" style="width:20px"/> &nbsp;<span>30x20</span><br>
														<img src="assets/img/icons/seen.png" style="width:20px"/> &nbsp;<span>Available</span>
													</div>
													<div class="col-md-2">
														<button class="btn ripple btn-block btn-1" style="color:white"><i class="fe fe-plus"></i>&nbsp;Add Sign</button>
													</div>
													
												</div>
												
											</div>
											
										</div>
									</div>
								</div>
								<!--End Grid modal -->
							</div>
						</div>
						<!-- End Row -->
					</div>
				</div>
			</div>
			<!-- End Main Content-->

			<!-- Main Footer-->
			<div class="main-footer text-center">
				<div class="container">
					<div class="row row-sm">
						<div class="col-md-12">
							<span>Copyright © 2022 <a href="#">PLAY/DOOH</a>. All rights reserved.</span>
						</div>
					</div>
				</div>
			</div>
			<!--End Footer-->

		</div>
		<!-- End Page -->

		<!-- Back-to-top -->
		<a href="#top" id="back-to-top" style="background-image: linear-gradient(to right, #34F5C5 0%, #2FBED0 51%, #3FA9F5 100%);border-color: #2FBED0;"><i class="fe fe-arrow-up"></i></a>

		<!-- Jquery js-->
		<script src="assets/plugins/jquery/jquery.min.js"></script>

		<!-- Bootstrap js-->
		<script src="assets/plugins/bootstrap/js/popper.min.js"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!-- Internal Chart.Bundle js-->
		<script src="assets/plugins/chart.js/Chart.bundle.min.js"></script>

		<!-- Peity js-->
		<script src="assets/plugins/peity/jquery.peity.min.js"></script>

		<!-- Internal Flot Chart js-->
		<script src="assets/plugins/jquery.flot/jquery.flot.js"></script>
		<script src="assets/plugins/jquery.flot/jquery.flot.pie.js"></script>
		<script src="assets/plugins/jquery.flot/jquery.flot.resize.js"></script>
		<script src="assets/js/chart.flot.js"></script>

		<!-- Select2 js-->
		<script src="assets/plugins/select2/js/select2.min.js"></script>
		<script src="assets/js/select2.js"></script>

		<!-- Perfect-scrollbar js -->
		<script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

		<!-- Sidemenu js -->
		<script src="assets/plugins/sidemenu/sidemenu.js" id="leftmenu"></script>

		<!-- Sidebar js -->
		<script src="assets/plugins/sidebar/sidebar.js"></script>

		<!-- Internal Apexchart js-->
		<script src="assets/js/apexcharts.js"></script>

		<!-- Internal Dashboard js-->
		<script src="assets/js/crypto-market.js"></script>

		<!-- Color Theme js -->
		<script src="assets/js/themeColors.js"></script>

		<!-- Sticky js -->
		<script src="assets/js/sticky.js"></script>

		<!-- Custom js -->
		<script src="assets/js/custom.js"></script>

		<script src="dashboard_features.js"></script>

		<script src="quotation_features.js"></script>

		<script src="chart_features.js"></script>
		
		<script src="map_features.js"></script>

		<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
		
		 <!-- prettier-ignore -->
    <script>(g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=`https://maps.${c}apis.com/maps/api/js?`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})
        ({key: "AIzaSyAfNwYDsoHn47AWav3I0-ht1Oh5ZnVnS0M", v: "weekly"});</script>
        
        <script async
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfNwYDsoHn47AWav3I0-ht1Oh5ZnVnS0M&callback=initMap">
</script>

	</body>
</html>